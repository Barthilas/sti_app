﻿using STIAPP.Model;

namespace STIAPP.Interfaces
{
    /// <summary>
    ///     Pokud možno tak uvádět v jednotkách nákup a prodej.
    /// </summary>
    public interface IBankParser
    {
        DailyCurrency Decode(string filepath);
    }
}