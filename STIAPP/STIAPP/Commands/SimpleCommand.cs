﻿using System;

namespace STIAPP.Commands
{
    public class SimpleCommand : BaseCommand
    {
        protected Action _execute;

        public SimpleCommand(Action execute)
            : this(execute, DefaultCanExecute)
        {
        }

        public SimpleCommand(Action execute, Predicate<object> canExecute)
            : base(canExecute)
        {
            if (execute == null) throw new ArgumentNullException("Vlastnost Execute není nastavena.");

            _execute = execute;
        }

        public override void Execute(object parameter)
        {
            //System.Windows.MessageBox.Show(_execute.Method.Name);
            _execute();
        }

        public void Destroy()
        {
            _canExecute = _ => false;
            _execute = () => { };
        }
    }
}