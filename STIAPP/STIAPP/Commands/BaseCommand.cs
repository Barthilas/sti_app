﻿using System;
using System.Windows.Input;

namespace STIAPP.Commands
{
    public abstract class BaseCommand : ICommand
    {
        protected Predicate<object> _canExecute;

        public BaseCommand(Predicate<object> canExecute)
        {
            if (canExecute == null) throw new ArgumentNullException("Vlastnost canExecute není nastavena.");
            _canExecute = canExecute;
        }

        public BaseCommand() : this(DefaultCanExecute)
        {
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
                CanExecuteChangedInternal += value;
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
                CanExecuteChangedInternal -= value;
            }
        }

        public virtual bool CanExecute(object parameter)
        {
            return _canExecute != null && _canExecute(parameter);
        }

        public virtual void Execute(object parameter)
        {
        }

        protected event EventHandler CanExecuteChangedInternal;

        protected void OnCanExecuteChanged()
        {
            var handler = CanExecuteChangedInternal;
            if (handler != null) handler.Invoke(this, EventArgs.Empty);
        }

        protected static bool DefaultCanExecute(object parameter)
        {
            return true;
        }
    }
}