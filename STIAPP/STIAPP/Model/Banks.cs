﻿using System.Collections.Generic;
using STIAPP.Parsers;

namespace STIAPP.Model
{
    /// <summary>
    ///     Holds all banks and its data.
    /// </summary>
    public static class Banks
    {
        public static List<Bank> BanksList = new List<Bank>();

        public static void InitBanks()
        {
            //CNB
            var cnb = new Bank("ČNB", new CNBParser(),
                @"http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt", ".csv");
            BanksList.Add(cnb);

            //CSOB
            var csob = new Bank("ČSOB", new CSOBParser(),
                @"https://www.csob.cz/portal/lide/kurzovni-listek/-/date/kurzy.txt", ".csv");
            BanksList.Add(csob);

            //Ceska sporitelna
            var sporitelna = new Bank("Česká spořitelna", new UniversalParser(), @"https://www.kurzy.cz/kurzy-men/kurzovni-listek/ceska-sporitelna/", ".html");
            BanksList.Add(sporitelna);

            //Raiffeisen
            var raif = new Bank("Raiffeisenbank", new UniversalParser(), @"https://www.kurzy.cz/kurzy-men/kurzovni-listek/raiffeisenbank", ".html");
            BanksList.Add(raif);

            //Komerční banka
            var kb = new Bank("Komerční banka", new UniversalParser(), @"https://www.kurzy.cz/kurzy-men/kurzovni-listek/komercni-banka/", ".html");
            BanksList.Add(kb);
        }
        private class HelperClass
        {
            public string Code { get; set; }
            public string Action { get; set; }

            public HelperClass(string c, string acti)
            {
                Code = c;
                Action = acti;
            }
        }
        public static void WhereToBuyOrSell(List<Bank> b)
        {
            var helper = new List<HelperClass>();
            var cnb = b[0]; //This has to be exact order.
            if (cnb.Evolution.Count == 1) return;
            var lastDay = cnb.Evolution[cnb.Evolution.Count - 1];
            var nextLastDay = cnb.Evolution[cnb.Evolution.Count - 2];

            if(lastDay == null || nextLastDay == null) return;
            //Identify conditions for buy or sell
            for (int i = 0; i < lastDay.DailyCurrencyList.Count; i++)
            {
                //Prodej měnu bance s maximální cenou za nákup
                if (lastDay.DailyCurrencyList[i].Sell > nextLastDay.DailyCurrencyList[i].Sell)
                {
                    //Stejné měny
                    if (lastDay.DailyCurrencyList[i].Name.CodeValue == nextLastDay.DailyCurrencyList[i].Name.CodeValue)
                    {
                        helper.Add(new HelperClass(lastDay.DailyCurrencyList[i].Name.CodeValue, "Sell"));
                    }
                }
                //Nakup měnu od banky s minimální cenou za prodej
                else
                {
                    //Stejné měny
                    if (lastDay.DailyCurrencyList[i].Name.CodeValue == nextLastDay.DailyCurrencyList[i].Name.CodeValue)
                    {
                        helper.Add(new HelperClass(lastDay.DailyCurrencyList[i].Name.CodeValue, "Buy"));
                    }

                }
            }
            //Find the best value, pun lol.
            foreach (var currencyHelper in helper)
            {
                double tmp = 0;
                double tmp2 = 999999;
                int currencyIndex = -1;
                int bankIndex = -1;
                string action = string.Empty;

                for (int i = 1; i < b.Count; i++)
                {
                    var lastDailyCurrency = b[i].Evolution[b[i].Evolution.Count-1];
                    (int _index, Currency _c) = lastDailyCurrency.FindCurrency(currencyHelper.Code);
                    if (_c != null)
                    {
                        if (currencyHelper.Action == "Sell")
                        {
                            if (_c.Buy > tmp)
                            {
                                tmp = _c.Buy;
                                currencyIndex = _index;
                                bankIndex = i;
                                action = "Prodej zde";
                            }
                        }
                        else
                        {
                            //TOTO FUNGUJE
                            if (_c.Sell < tmp2)
                            {
                                tmp2 = _c.Sell;
                                currencyIndex = _index;
                                bankIndex = i;
                                action = "Nakup zde";
                            }
                        }
                    }

                }
                //evaluate
                if (bankIndex != -1)
                {
                    b[bankIndex].Evolution[b[bankIndex].Evolution.Count - 1].DailyCurrencyList[currencyIndex].Info.Action =
                        action;
                }
            }

        }
    }
}