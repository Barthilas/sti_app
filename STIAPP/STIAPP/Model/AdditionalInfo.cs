﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STIAPP.Model
{
    public class AdditionalInfo
    {
        /// <summary>
        /// Tells the user to buy or sell. TODO
        /// </summary>
        public string Action { get; set; }
        public double BuyChange { get; set; }
        public double SellChange { get; set; }

        public AdditionalInfo()
        {
            Action = string.Empty;
            BuyChange = 0.00;
            SellChange = 0.00;
        }

        public void AddInfo(double buy, double sell)
        {
            BuyChange = Math.Round(buy,4);
            SellChange = Math.Round(sell,4);
        }
    }
}
