﻿using STIAPP.Enums;

namespace STIAPP.Model
{
    /// <summary>
    ///     MODEL - Takes a look at one exact currency.
    /// </summary>
    public class Currency
    {
        public Currency(SupportedCurrencies _currency, float _buy, float _sell, int amount)
        {
            Name = _currency;
            Sell = _sell;
            Amount = amount;
            Buy = _buy;
            Info = new AdditionalInfo();

        }

        public SupportedCurrencies Name { get; set; }
        public float Sell { get; set; }
        public float Buy { get; set; }
        public int Amount { get; set; }
        public AdditionalInfo Info { get; set; }
    }
}