﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using STIAPP.Interfaces;

namespace STIAPP.Model
{
    public class Bank
    {
        public Bank(string name, IBankParser parser, string downloadLink, string fileExtension)
        {
            Parser = parser;
            DownloadLink = downloadLink;
            BankName = name;
            Evolution = new ObservableCollection<DailyCurrency>();
            FileExtension = fileExtension;
        }

        //public bool UpdateCheck()
        //{
        //    foreach (var daily in Evolution)
        //    {
        //        if (daily.Published == DateTime.Today)
        //        {
        //            return false;
        //        }
        //    }

        //    return true;
        //}
        public bool UpdateCheck(DateTime d)
        {
            foreach (var daily in Evolution)
            {
                if (daily.Published == d)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Adds additional info for dailyCurrencies If possible. Newer - older.
        /// </summary>
        /// <returns></returns>
        public void AdditionalInfoAvailable()
        {
            //More than 1 record 
            if (Evolution.Count>1)
            {
                var older = this.Evolution[Evolution.Count-2];
                var newer = this.Evolution[Evolution.Count-1];
                for (int i = 0; i < older.DailyCurrencyList.Count; i++)
                {
                    double changeBuy = newer.DailyCurrencyList[i].Buy - older.DailyCurrencyList[i].Buy;
                    double changeSell = newer.DailyCurrencyList[i].Sell - older.DailyCurrencyList[i].Sell;

                    newer.DailyCurrencyList[i].Info.AddInfo(changeBuy, changeSell);
                }
            }
        }
        public IBankParser Parser { get; set; }
        public string DownloadLink { get; set; }
        public string BankName { get; set; }
        public string FileExtension { get; set; }
        public ObservableCollection<DailyCurrency> Evolution { get; set; }
    }
}