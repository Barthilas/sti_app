﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using STIAPP.Enums;

namespace STIAPP.Model
{
    /// <summary>
    ///     MODEL - Keeps info about all daily currencies from bank
    /// </summary>
    public class DailyCurrency
    {
        public DailyCurrency()
        {
            DailyCurrencyList = new ObservableCollection<Currency>();
        }

        public DailyCurrency(DateTime t)
        {
            DailyCurrencyList = new ObservableCollection<Currency>();
            Published = t;
        }

        public (int index, Currency c) FindCurrency(string code)
        {
            code = code.ToUpper();//normalize
            int index = -1;
            Currency c = null;
            for (int i = 0; i < DailyCurrencyList.Count; i++)
            {
                if (DailyCurrencyList[i].Name.CodeValue == code)
                {
                    index = i;
                    c = DailyCurrencyList[i];
                }
            }
            return (index, c);
        }

        public ObservableCollection<Currency> DailyCurrencyList { get; set; }
        public DateTime Published { get; set; }
    }
}