﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using AutoUpdaterDotNET;
using STIAPP.Enums;
using STIAPP.Model;
using STIAPP.Services;
using STIAPP.Utils;

namespace STIAPP.View
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //NECCESARY TO RUN !!!
            ExchangeRateUpdater e = ExchangeRateUpdater.Instance;
            var k = ProgramVersionUpdater.Instance;

            #region thrash
            //####################

            //Everything under is for testing

            //DateTime date = DateTime.Now;
            //DateTime ll = DateTimeUtils.AddWorkdays(date, 1);
            //DateTime lll = DateTimeUtils.AddWorkdays(date, 2);

            //InternetAvailability.IsInternetAvailable();
            //var lk = Banks.BanksList[0].Evolution[0].DailyCurrencyList[1].Name;


            //DataBackup.ExportXML(Banks.BanksList, ProgramVariables.XmlBanksFilePath);
            //DataBackup.ImportXML(ProgramVariables.XmlBanksFilePath);

            //var len = Banks.BanksList[0].Evolution.Count;

            //var l = Banks.BanksList[0].Evolution[len - 1].DailyCurrencyList[1].Name.;


            #endregion



        }

        private void UIElement_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //until we had a StaysOpen glag to Drawer, this will help with scroll bars
            var dependencyObject = Mouse.Captured as DependencyObject;
            while (dependencyObject != null)
            {
                if (dependencyObject is ScrollBar) return;
                dependencyObject = VisualTreeHelper.GetParent(dependencyObject);
            }

            MenuButton.IsChecked = false;
        }
        private void Close_Application(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Gitlab_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/Barthilas/sti_app");
        }

        /// <summary>
        /// Save data when closing application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DataBackup.ExportXML(Banks.BanksList, ProgramVariables.XmlBanksFilePath);
        }
        /// <summary>
        ///Load past data when app is finished loading.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Initialized(object sender, EventArgs e)
        {
            Banks.InitBanks();
            DataBackup.ImportXML(ProgramVariables.XmlBanksFilePath);
        }

        private void ListBoxItem_EventUpdateCurrency(object sender, MouseButtonEventArgs e)
        {
            ExchangeRateUpdater.Check();
        }
        private void ListBoxItem_EventUpdateProgram(object sender, MouseButtonEventArgs e)
        {
            AutoUpdater.Mandatory = true;
            ProgramVersionUpdater.Check();
        }
    }
}