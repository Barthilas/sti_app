﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Threading;
using AutoUpdaterDotNET;
using STIAPP.Enums;
using STIAPP.Model;
using STIAPP.Utils;

namespace STIAPP.Services
{
    /// <summary>
    ///     Každý nový pracovní den načíst nové data. Nejlépe ne v hlavním vlákně.
    /// </summary>
    public class ExchangeRateUpdater
    {
        private readonly DispatcherTimer _exchangeRateDispatcherTimer = null;

        public static void Check()
        {
            //Download files and process
            var task = Task.Factory.StartNew(() =>
            {
                using (var client = new WebClient())
                {
                    foreach (var banka in Banks.BanksList)
                    {
                        if (!InternetAvailability.IsInternetAvailable()) continue;
                        var completePath = Path.Combine(ProgramVariables.TemporaryFolderPath,
                            banka.BankName + banka.FileExtension);
                        if (banka.DownloadLink != null)
                        {
                            client.DownloadFile(banka.DownloadLink, completePath);
                        }
                        var dailyCurrencyBank = banka.Parser.Decode(completePath);
                        //If exists record for today do not add.
                        if (banka.UpdateCheck(dailyCurrencyBank.Published))
                        {
                            banka.Evolution.Add(dailyCurrencyBank);
                            //Append additional info
                            banka.AdditionalInfoAvailable();
                        }
                    }
                }
                //Výhodnost append
                Banks.WhereToBuyOrSell(Banks.BanksList);
            });

            //Folder does not exist -> create.
            if (!File.Exists(ProgramVariables.TemporaryFolderPath))
                Directory.CreateDirectory(ProgramVariables.TemporaryFolderPath);
            //Folder exists -> delete first.
            else
                FileUtils.DeleteFolderAndFiles(ProgramVariables.TemporaryFolderPath);

            task.Wait();
            //Eventually delete
            Utils.FileUtils.DeleteFolderAndFiles(Utils.ProgramVariables.TemporaryFolderPath);


        }

        private static readonly Lazy<ExchangeRateUpdater>
            Lazy =
                new Lazy<ExchangeRateUpdater>
                    (() => new ExchangeRateUpdater());

        private ExchangeRateUpdater()
        {
            //First time update when run for the first time.
            Check();

            //Daily working days updater -> 24hrs
            DateTime tomorrow = DateTimeUtils.AddWorkdays(DateTime.Now, 1);
            var nextUpdateInterval = tomorrow.Subtract(DateTime.Now);
            _exchangeRateDispatcherTimer = new DispatcherTimer();
            _exchangeRateDispatcherTimer.Interval = nextUpdateInterval;
            _exchangeRateDispatcherTimer.Tick += delegate { Check(); };
                _exchangeRateDispatcherTimer.Start();
        }

        public static ExchangeRateUpdater Instance => Lazy.Value;
    }
}