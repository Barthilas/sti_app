﻿using System;
using System.Windows.Threading;
using AutoUpdaterDotNET;
using STIAPP.Utils;

namespace STIAPP.Services
{
    /// <summary>
    ///     AUTOUPDATER.NET
    /// </summary>
    public sealed class ProgramVersionUpdater
    {

        public static void Check()
        {
            if (!InternetAvailability.IsInternetAvailable()) return;
            AutoUpdater.Start(ProgramVariables.XmlUpdateFile);
        }

        private static readonly Lazy<ProgramVersionUpdater>
            lazy =
                new Lazy<ProgramVersionUpdater>
                    (() => new ProgramVersionUpdater());

        private ProgramVersionUpdater()
        {
            Check();

            var timer = new DispatcherTimer {Interval = TimeSpan.FromMinutes(2)};
            timer.Tick += delegate { Check(); };
            timer.Start();
        }

        public static ProgramVersionUpdater Instance => lazy.Value;
    }
}