﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Microsoft.Win32;
using OfficeOpenXml;
using STIAPP.Model;
using STIAPP.ViewModel;

namespace STIAPP.Services
{
    public static class ExcelService
    {
        /// <summary>
        ///     https://www.codebyamir.com/blog/create-excel-files-in-c-sharp
        /// </summary>
        public static void Export()
        {
            var banksList = Banks.BanksList;
            if (banksList == null) return;
            var saveFileDialog = new SaveFileDialog
            {
                Filter =
                    "XLSX File | *.xlsx | CSV File |*.csv | All files |*.*"
            };

            if (saveFileDialog.ShowDialog() != true) return;

            using (var excel = new ExcelPackage())
            {
                var headerRow = new List<string[]>
                {
                    new[] {"Název měny", "Kod měny", "Množství", "Nákup", "Prodej"}
                };
                foreach (var bank in banksList)
                {
                    var headerRange = "A1:" + char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    excel.Workbook.Worksheets.Add(bank.BankName);
                    var worksheet = excel.Workbook.Worksheets[bank.BankName];
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    var dailyLatest = bank.Evolution[bank.Evolution.Count - 1];
                    for (var i = 0; i < dailyLatest.DailyCurrencyList.Count - 1; i++)
                    {
                        var daily = dailyLatest.DailyCurrencyList[i];
                        var dataRow = new List<string[]>
                        {
                            new[]
                            {
                                daily.Name.TextValue, daily.Name.CodeValue, daily.Amount.ToString(),
                                daily.Buy.ToString(), daily.Sell.ToString()
                            }
                        };
                        var dataRange = string.Format("A{0}:{1}{2}", i + 2,
                            char.ConvertFromUtf32(headerRow[0].Length + 64), i + 2);
                        worksheet.Cells[dataRange].LoadFromArrays(dataRow);
                    }
                }

                var excelFile = new FileInfo(saveFileDialog.FileName);
                excel.SaveAs(excelFile);

                #region example

                //excel.Workbook.Worksheets.Add("ČSOB");
                //excel.Workbook.Worksheets.Add("ČNB");
                //excel.Workbook.Worksheets.Add("Česká spořitelna");
                //excel.Workbook.Worksheets.Add("Raiffeisenbank");

                //var headerRow = new List<string[]>()
                //{
                //    new string[] {"ID", "First Name", "Last Name", "DOB"}
                //};

                //// Determine the header range (e.g. A1:D1)
                //string headerRange = "A1:" + char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                //// Target a worksheet
                //var worksheet = excel.Workbook.Worksheets["Worksheet1"];

                //// Popular header row data
                //worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                //FileInfo excelFile = new FileInfo(saveFileDialog.FileName);
                //excel.SaveAs(excelFile);

                #endregion
            }
        }

        public static void ExportSelected(ObservableCollection<CheckBoxListItem> _checkBoxes)
        {
            List<string> currencyNames = new List<string>();
            //Include only this currencies.
            foreach (var currency in _checkBoxes)
            {
                if (currency.Checked)
                {
                    currencyNames.Add(currency.Text);
                }
            }

            var saveFileDialog = new SaveFileDialog
            {
                Filter =
                    "XLSX File | *.xlsx | CSV File |*.csv | All files |*.*"
            };

            saveFileDialog.FileName = "ExportedCurrencies";

            if (saveFileDialog.ShowDialog() != true) return;

            

            using (var excel = new ExcelPackage())
            {
                var headerRow = new List<string[]>
                {
                    new[] {"Název měny", "Kod měny", "Množství", "Nákup", "Prodej", "Akce"}
                };
                foreach (var bank in Banks.BanksList)
                {
                    //Create headers
                    var headerRange = "A1:" + char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    excel.Workbook.Worksheets.Add(bank.BankName);
                    var worksheet = excel.Workbook.Worksheets[bank.BankName];
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                    //Append relevant data
                    var dailyLatest = bank.Evolution[bank.Evolution.Count - 1];
                    var actualIndex = 0;
                    for (var i = 0; i < dailyLatest.DailyCurrencyList.Count - 1; i++)
                    {
                        var daily = dailyLatest.DailyCurrencyList[i];
                        if (currencyNames.Contains(daily.Name.TextValue))
                        {
                            var dataRow = new List<string[]>
                            {
                                new[]
                                {
                                    daily.Name.TextValue, daily.Name.CodeValue, daily.Amount.ToString(),
                                    daily.Buy.ToString(), daily.Sell.ToString(), daily.Info.Action
                                }
                            };

                            var dataRange = string.Format("A{0}:{1}{2}", actualIndex + 2,
                                char.ConvertFromUtf32(headerRow[0].Length + 64), actualIndex + 2);
                            worksheet.Cells[dataRange].LoadFromArrays(dataRow);
                            actualIndex++;
                        }
                    }
                    //Save excel
                    var excelFile = new FileInfo(saveFileDialog.FileName);
                    excel.SaveAs(excelFile);
                }
            }
        }
    }
}