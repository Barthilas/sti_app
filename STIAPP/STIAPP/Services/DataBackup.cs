﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using STIAPP.Enums;
using STIAPP.Model;

namespace STIAPP.Services
{
    public static class DataBackup
    {
        public static void ExportXML(List<Bank> banks, string savePath)
        {
            var doc = new XmlDocument();
            var xmlDeclaration = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(xmlDeclaration);
            var banksElement = doc.CreateElement("Banks");
            banksElement.SetAttribute("Count", banks.Count.ToString());
            doc.AppendChild(banksElement);
            foreach (var banka in banks)
            {
                var bankElement = doc.CreateElement("Bank");
                bankElement.SetAttribute("Name", banka.BankName);
                banksElement.AppendChild(bankElement);
                foreach (var dailyCurrency in banka.Evolution)
                {
                    var currencyIndicesElement = doc.CreateElement("CurrencyIndices");
                    currencyIndicesElement.SetAttribute("Published", dailyCurrency.Published.ToShortDateString());
                    currencyIndicesElement.SetAttribute("Count", dailyCurrency.DailyCurrencyList.Count.ToString());
                    bankElement.AppendChild(currencyIndicesElement);
                    foreach (var currency in dailyCurrency.DailyCurrencyList)
                    {
                        var currencyElement = doc.CreateElement("Currency");
                        currencyElement.SetAttribute("Code", currency.Name.CodeValue);
                        currencyElement.SetAttribute("Amount", currency.Amount.ToString());
                        currencyElement.SetAttribute("Buy", currency.Buy.ToString());
                        currencyElement.SetAttribute("Sell", currency.Sell.ToString());
                        currencyIndicesElement.AppendChild(currencyElement);
                    }
                }
            }
            doc.Save(savePath);
        }
        /// <summary>
        /// Banks have to be inicialized before this method.
        /// </summary>
        /// <param name="filePath"></param>
        public static void ImportXML(string filePath)
        {
            if(!File.Exists(filePath))return;
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(filePath);
            var banks = xmlDoc.GetElementsByTagName("Bank");
            foreach (var bankNode in banks)
            {
                var currencies = (bankNode as XmlElement).ChildNodes;
                foreach (XmlElement curr_daily in currencies)
                {
                    var published = DateTime.Parse(curr_daily.Attributes["Published"].Value);
                    var bankName = (bankNode as XmlElement).Attributes["Name"].Value;
                    var matchedBank = Banks.BanksList.FirstOrDefault(t => t.BankName == bankName);
                    var dailyCurrency = new DailyCurrency(published);
                    var currencyList = curr_daily.ChildNodes;
                    foreach (var currency in currencyList)
                    {
                        var currency_xml = currency as XmlElement;
                        var code = currency_xml.Attributes["Code"].Value;
                        var amount = int.Parse(currency_xml.Attributes["Amount"].Value);
                        var buy = float.Parse(currency_xml.Attributes["Buy"].Value);
                        var sell = float.Parse(currency_xml.Attributes["Sell"].Value);
                        var c = new Currency(SupportedCurrenciesFactory.Create(code), buy, sell, amount);
                        dailyCurrency.DailyCurrencyList.Add(c);
                    }

                    if (matchedBank.UpdateCheck(dailyCurrency.Published))
                    {
                        matchedBank.Evolution.Add(dailyCurrency);
                    }
                }
            }
        }
    }
}
