﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Management.Instrumentation;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Media;
using MaterialDesignThemes.Wpf;
using STIAPP.Commands;
using STIAPP.Model;
using STIAPP.Services;
using LiveCharts;
using LiveCharts.Wpf;
using System.Threading;

namespace STIAPP.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private int _switchView;
        // private SeriesCollection _aud;
        private List<string> _labels;
        private ObservableCollection<CheckBoxListItem> _checkBoxes;
        private ObservableCollection<ChartListItem> _charts;
        private string _range = "Všechna";
        private Timer timer;

        public string AssemblyVersion => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public SimpleCommand ExportExcelCommand { get; set; }

        public MainWindowViewModel()
        {
            ExportExcelCommand = new SimpleCommand(ExcelSelectedExport);
            // _aud = new SeriesCollection();
            _labels = new List<string>();
            _checkBoxes = new ObservableCollection<CheckBoxListItem>();
            // _charts = new ObservableCollection<ChartListItem>();
            timer = new Timer(UpdateProperty, null, 100000, 100000);
        }

        private void UpdateProperty(object state)
        {
            lock (this)
            {
                NotifyPropertyChanged("Charts");
            }
        }

        private void ExcelSelectedExport()
        {
            ExcelService.ExportSelected(_checkBoxes);
        }

        //number of active usercontrol 
        public int SwitchView
        {
            get
            {
                return _switchView;
            }
            set
            {
                this._switchView = value;
                NotifyPropertyChanged("SwitchView");
            }
        }

        //selected data range
        public string SelectedRange
        {
            get { return _range; }
            set
            {
                _range = value;
                NotifyPropertyChanged("SelectedRange");
                NotifyPropertyChanged("Charts");
                NotifyPropertyChanged("Labels");
            }
        }

        #region GetDailyCurrencyFromBanks

        public ObservableCollection<Currency> GetDailyCNB
        {
            get
            {
                var evolution = Banks.BanksList[0].Evolution.Count - 1;
                return Banks.BanksList[0].Evolution[evolution].DailyCurrencyList;
            }
        }

        public ObservableCollection<Currency> GetDailyCSOB
        {
            get
            {
                var evolution = Banks.BanksList[1].Evolution.Count - 1;
                return Banks.BanksList[1].Evolution[evolution].DailyCurrencyList;
            }
        }

        public ObservableCollection<Currency> GetDailyCS
        {
            get
            {
                var evolution = Banks.BanksList[2].Evolution.Count - 1;
                return Banks.BanksList[2].Evolution[evolution].DailyCurrencyList;
            }
        }

        public ObservableCollection<Currency> GetDailyRB
        {
            get
            {
                var evolution = Banks.BanksList[3].Evolution.Count - 1;
                return Banks.BanksList[3].Evolution[evolution].DailyCurrencyList;
            }
        }

        public ObservableCollection<Currency> GetDailyKB
        {
            get
            {
                var evolution = Banks.BanksList[4].Evolution.Count - 1;
                return Banks.BanksList[4].Evolution[evolution].DailyCurrencyList;
            }
        }

        #endregion

        public ObservableCollection<CheckBoxListItem> Currencies
        {
            get
            {
                foreach (var currency in Banks.BanksList[0].Evolution[0].DailyCurrencyList)
                {
                    _checkBoxes.Add(new CheckBoxListItem(false, currency.Name.TextValue));
                }
                return _checkBoxes;
            }
            set
            {
                NotifyPropertyChanged("Currencies");
            }
        }

        public List<string> Labels
        {
            get
            {
                /*
                foreach (var day in Banks.BanksList[1].Evolution)
                {
                    this._labels.Add(day.Published.ToShortDateString());
                }
                */
                return this._labels;
            }
            set
            {
                NotifyPropertyChanged("Labels");
            }
        }

        public SolidColorBrush BankStrokeColor(string bank)
        {
            SolidColorBrush brush = Brushes.White;
            switch (bank)
            {
                case "ČNB":
                    brush = Brushes.Black;
                    break;
                case "ČSOB":
                    brush = Brushes.DarkTurquoise;
                    break;
                case "Česká spořitelna":
                    brush = Brushes.DeepPink;
                    break;
                case "Raiffeisenbank":
                    brush = Brushes.Orange;
                    break;
                case "Komerční banka":
                    brush = Brushes.SlateGray;
                    break;
                default:
                    brush = Brushes.DarkOrchid;
                    break;
            }

            return brush;
        }

        // data pro všechny generované grafy
        public ObservableCollection<ChartListItem> Charts
        {
            get
            {
                this._labels.Clear();
                DateTime today = DateTime.Today;
                DateTime startDay;
                if (this._range == "Posledních 30 dní")
                {
                    startDay = DateTime.Today.AddDays(-30);
                }
                else if (this._range == "Posledních 7 dní")
                {
                    startDay = DateTime.Today.AddDays(-7);
                }
                else
                {
                    startDay = new DateTime(1970, 1, 1);
                }
                _charts = new ObservableCollection<ChartListItem>();
                foreach (var currency in _checkBoxes)
                {
                    var serieCollectionBuy = new SeriesCollection();
                    var serieCollectionSell = new SeriesCollection();
                    string title = string.Empty;
                    string action = string.Empty;
                    foreach (var bank in Banks.BanksList)
                    {
                        var lineSeriesBuy = new LineSeries();
                        lineSeriesBuy.Stroke = BankStrokeColor(bank.BankName);
                        var lineSeriesSell = new LineSeries();
                        lineSeriesSell.Stroke = lineSeriesBuy.Stroke;
                        var chartValueBuy = new ChartValues<double>();
                        var chartValueSell = new ChartValues<double>();
                        bool addBuy = false;
                        bool addSell = false;
                        foreach (var bankEvolution in bank.Evolution)
                        {
                            int result = DateTime.Compare(startDay, bankEvolution.Published);
                            if (result <= 0)
                            {
                                
                                if (bank.BankName == "ČSOB")
                                {
                                    this._labels.Add(bankEvolution.Published.ToShortDateString());
                                }
                                
                                foreach (var currencyList in bankEvolution.DailyCurrencyList)
                                {
                                    if (currencyList.Name.TextValue == currency.Text)
                                    {
                                        if (bank.BankName != "ČNB")
                                        {
                                            //currencyList.Info.Action;
                                            //action += currencyList.Info.Action;
                                            if (currencyList.Info.Action.Length > 0)
                                            {
                                                action = currencyList.Info.Action + " -> " + bank.BankName;
                                            }
                                            lineSeriesSell.Title = bank.BankName;
                                            chartValueSell.Add(Math.Round(currencyList.Sell, 4));
                                            title = currencyList.Name.TextValue + " - " + currencyList.Name.CodeValue;
                                            addSell = true;

                                            lineSeriesBuy.Title = bank.BankName;
                                            chartValueBuy.Add(Math.Round(currencyList.Buy, 4));
                                            addBuy = true;
                                        }
                                    }
                                }
                            }
                        }

                        if (addBuy)
                        {
                            lineSeriesBuy.Values = chartValueBuy;
                            lineSeriesBuy.Fill = Brushes.Transparent;
                            serieCollectionBuy.Add(lineSeriesBuy);
                        }

                        if (addSell)
                        {
                            lineSeriesSell.Values = chartValueSell;
                            lineSeriesSell.Fill = Brushes.Transparent;
                            serieCollectionSell.Add(lineSeriesSell);
                        }
                        
                    }
                    _charts.Add(new ChartListItem(currency, serieCollectionBuy, serieCollectionSell, Labels, action));
                }

                return _charts;
            }
        }

        /* graf pro každou měnu ručně -> otročina
        public SeriesCollection AUD
        {

            get
            {
                foreach (var bank in Banks.BanksList)
                {
                    var lineSeries = new LineSeries();
                    lineSeries.Title = bank.BankName;
                    var chartValue = new ChartValues<float>();
                    foreach (var day in bank.Evolution)
                    {
                        chartValue.Add(day.DailyCurrencyList[0].Sell);
                        //lineSeries.Values = new ChartValues<float>(day.DailyCurrencyList);
                    }

                    lineSeries.Values = chartValue;
                    lineSeries.Fill = Brushes.Transparent;
                    _aud.Add(lineSeries);
                }
                return _aud;
            }
            set
            {
                _aud = value;
                NotifyPropertyChanged("AUD");
            }
        }
        */

        #region propertychanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));

            }
        }
        #endregion
    }

    public class CheckBoxListItem : INotifyPropertyChanged
    {
        private bool _checked;
        private string _text;
        public CheckBoxListItem(bool check, string text)
        {
            _checked = check;
            _text = text;
        }

        public bool Checked
        {
            get { return _checked; }
            set
            {
                this._checked = value;
                NotifyPropertyChanged("Checked");
            }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                NotifyPropertyChanged("Text");
            }
        }

        #region propertychanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));

            }
        }
        #endregion
    }

    public class ChartListItem : INotifyPropertyChanged
    {
        private SeriesCollection _buy;
        private SeriesCollection _sell;
        private CheckBoxListItem _item;
        private List<string> _labels;
        private string _action;

        public ChartListItem(CheckBoxListItem item, SeriesCollection buy, SeriesCollection sell, List<string> labels, string action)
        {
            this._item = item;
            this._buy = buy;
            this._sell = sell;
            this._labels = labels;
            this._action = action;
        }

        public CheckBoxListItem Item
        {
            get { return this._item; }
            set
            {
                this._item = value;
                NotifyPropertyChanged("Item");
            }
        }

        public SeriesCollection Buy
        {
            get { return this._buy; }
            set
            {
                this._buy = value;
                NotifyPropertyChanged("Buy");
            }
        }

        public SeriesCollection Sell
        {
            get { return this._sell; }
            set
            {
                this._sell = value;
                NotifyPropertyChanged("Sell");
            }
        }

        public List<string> Labels
        {
            get { return this._labels; }
            set
            {
                this._labels = value;
                NotifyPropertyChanged("Currency");
            }
        }

        public string Action
        {
            get { return this._action; }
            set
            {
                this._action = value;
                NotifyPropertyChanged("Action");
            }
        }

        #region propertychanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));

            }
        }
        #endregion
    }
}