﻿namespace STIAPP.Enums
{
    public class SupportedCurrencies
    {
        private SupportedCurrencies(string value, string code)
        {
            TextValue = value;
            CodeValue = code;
        }
        public string TextValue { get; set; }
        public string CodeValue { get; set; }

        #region FUCKTON_OF_VALUES

        public static SupportedCurrencies AED => new SupportedCurrencies("AED", "AED");
        public static SupportedCurrencies AFN => new SupportedCurrencies("Afghanistan Afghani","AFN");
        public static SupportedCurrencies ALL => new SupportedCurrencies("Albania Lek", "ALL");
        public static SupportedCurrencies AMD => new SupportedCurrencies("Armenia Dram", "AMD");
        public static SupportedCurrencies ANG => new SupportedCurrencies("Netherlands Antilles Guilder","ANG");
        public static SupportedCurrencies AOA => new SupportedCurrencies("Angola Kwanza", "AOA");
        public static SupportedCurrencies ARS => new SupportedCurrencies("Argentina Peso", "ARS");
        public static SupportedCurrencies AUD => new SupportedCurrencies("Australia Dollar", "AUD");
        public static SupportedCurrencies AWG => new SupportedCurrencies("Aruba Guilder", "AWG");
        public static SupportedCurrencies AZN => new SupportedCurrencies("Azerbaijan New Manat", "AZN");
        public static SupportedCurrencies BAM => new SupportedCurrencies("Bosnia and Herzegovina Convertible Marka", "BAM");
        public static SupportedCurrencies BBD => new SupportedCurrencies("Barbados Dollar", "BBD");
        public static SupportedCurrencies BDT => new SupportedCurrencies("Bangladesh Taka", "BDT");
        public static SupportedCurrencies BGN => new SupportedCurrencies("Bulgaria Lev", "BGN");
        public static SupportedCurrencies BHD => new SupportedCurrencies("Bahrain Dinar", "BHD");
        public static SupportedCurrencies BIF => new SupportedCurrencies("Burundi Franc", "BIF");
        public static SupportedCurrencies BMD => new SupportedCurrencies("Bermuda Dollar", "BMD");
        public static SupportedCurrencies BND => new SupportedCurrencies("Brunei Darussalam Dollar", "BND");
        public static SupportedCurrencies BOB => new SupportedCurrencies("Bolivia Bolíviano", "BOB");
        public static SupportedCurrencies BRL => new SupportedCurrencies("Brazil Real", "BRL");
        public static SupportedCurrencies BSD => new SupportedCurrencies("Bahamas Dollar", "BSD");
        public static SupportedCurrencies BTN => new SupportedCurrencies("Bhutan Ngultrum", "BTN");
        public static SupportedCurrencies BWP => new SupportedCurrencies("Botswana Pula", "BWP");
        public static SupportedCurrencies BYR => new SupportedCurrencies("Belarus Ruble", "BYR");
        public static SupportedCurrencies BZD => new SupportedCurrencies("Belize Dollar", "BZD");
        public static SupportedCurrencies CAD => new SupportedCurrencies("Canada Dollar", "CAD");
        public static SupportedCurrencies CDF => new SupportedCurrencies("Congo/Kinshasa Franc", "CDF");
        public static SupportedCurrencies CHF => new SupportedCurrencies("Switzerland Franc", "CHF");
        public static SupportedCurrencies CLP => new SupportedCurrencies("Chile Peso", "CLP");
        public static SupportedCurrencies CNY => new SupportedCurrencies("China Yuan Renminbi", "CNY");
        public static SupportedCurrencies COP => new SupportedCurrencies("Colombia Peso", "COP");
        public static SupportedCurrencies CRC => new SupportedCurrencies("Costa Rica Colon", "CRC");
        public static SupportedCurrencies CUC => new SupportedCurrencies("Cuba Convertible Peso", "CUC");
        public static SupportedCurrencies CUP => new SupportedCurrencies("Cuba Peso", "CUP");
        public static SupportedCurrencies CVE => new SupportedCurrencies("Cape Verde Escudo", "CVE");
        public static SupportedCurrencies CZK => new SupportedCurrencies("Czech Republic Koruna", "CZK");
        public static SupportedCurrencies DJF => new SupportedCurrencies("Djibouti Franc", "DJF");
        public static SupportedCurrencies DKK => new SupportedCurrencies("Denmark Krone", "DKK");
        public static SupportedCurrencies DOP => new SupportedCurrencies("Dominican Republic Peso", "DOP");
        public static SupportedCurrencies DZD => new SupportedCurrencies("Algeria Dinar", "DZP");
        public static SupportedCurrencies EGP => new SupportedCurrencies("Egypt Pound", "EGP");
        public static SupportedCurrencies ERN => new SupportedCurrencies("Eritrea Nakfa", "ERN");
        public static SupportedCurrencies ETB => new SupportedCurrencies("Ethiopia Birr", "ETB");
        public static SupportedCurrencies EUR => new SupportedCurrencies("Euro Member Countries", "EUR");
        public static SupportedCurrencies FJD => new SupportedCurrencies("Fiji Dollar", "FJD");
        public static SupportedCurrencies FKP => new SupportedCurrencies("Falkland Islands (Malvinas) Pound", "FKP");
        public static SupportedCurrencies GBP => new SupportedCurrencies("United Kingdom Pound", "GBP");
        public static SupportedCurrencies GEL => new SupportedCurrencies("Georgia Lari", "GEL");
        public static SupportedCurrencies GGP => new SupportedCurrencies("Guernsey Pound", "GGP");
        public static SupportedCurrencies GHS => new SupportedCurrencies("Ghana Cedi", "GHS");
        public static SupportedCurrencies GIP => new SupportedCurrencies("Gibraltar Pound", "GIP");
        public static SupportedCurrencies GMD => new SupportedCurrencies("Gambia Dalasi", "GMD");
        public static SupportedCurrencies GNF => new SupportedCurrencies("Guinea Franc", "GNF");
        public static SupportedCurrencies GTQ => new SupportedCurrencies("Guatemala Quetzal", "GTQ");
        public static SupportedCurrencies GYD => new SupportedCurrencies("Guyana Dollar", "GYD");
        public static SupportedCurrencies HKD => new SupportedCurrencies("Hong Kong Dollar", "HKD");
        public static SupportedCurrencies HNL => new SupportedCurrencies("Honduras Lempira", "HNL");
        public static SupportedCurrencies HRK => new SupportedCurrencies("Croatia Kuna", "HRK");
        public static SupportedCurrencies HTG => new SupportedCurrencies("Haiti Gourde", "HTG");
        public static SupportedCurrencies HUF => new SupportedCurrencies("Hungary Forint", "HUF");
        public static SupportedCurrencies IDR => new SupportedCurrencies("Indonesia Rupiah", "IDR");
        public static SupportedCurrencies ILS => new SupportedCurrencies("Israel Shekel", "ILS");
        public static SupportedCurrencies IMP => new SupportedCurrencies("Isle of Man Pound", "IMP");
        public static SupportedCurrencies INR => new SupportedCurrencies("India Rupee", "INR");
        public static SupportedCurrencies IQD => new SupportedCurrencies("Iraq Dinar", "IQD");
        public static SupportedCurrencies IRR => new SupportedCurrencies("Iran Rial", "IRR");
        public static SupportedCurrencies ISK => new SupportedCurrencies("Iceland Krona", "ISK");
        public static SupportedCurrencies JEP => new SupportedCurrencies("Jersey Pound", "JEP");
        public static SupportedCurrencies JMD => new SupportedCurrencies("Jamaica Dollar", "JMD");
        public static SupportedCurrencies JOD => new SupportedCurrencies("Jordan Dinar", "JOD");
        public static SupportedCurrencies JPY => new SupportedCurrencies("Japan Yen", "JPY");
        public static SupportedCurrencies KES => new SupportedCurrencies("Kenya Shilling", "KES");
        public static SupportedCurrencies KGS => new SupportedCurrencies("Kyrgyzstan Som", "KGS");
        public static SupportedCurrencies KHR => new SupportedCurrencies("Cambodia Riel", "KHR");
        public static SupportedCurrencies KMF => new SupportedCurrencies("Comoros Franc", "KMF");
        public static SupportedCurrencies KPW => new SupportedCurrencies("Korea (North) Won", "KPW");
        public static SupportedCurrencies KRW => new SupportedCurrencies("Korea (South) Won", "KRW");
        public static SupportedCurrencies KWD => new SupportedCurrencies("Kuwait Dinar", "KWD");
        public static SupportedCurrencies KYD => new SupportedCurrencies("Cayman Islands Dollar", "KYD");
        public static SupportedCurrencies KZT => new SupportedCurrencies("Kazakhstan Tenge", "KZT");
        public static SupportedCurrencies LAK => new SupportedCurrencies("Laos Kip", "LAK");
        public static SupportedCurrencies LBP => new SupportedCurrencies("Lebanon Pound", "LBP");
        public static SupportedCurrencies LKR => new SupportedCurrencies("Sri Lanka Rupee", "LKR");
        public static SupportedCurrencies LRD => new SupportedCurrencies("Liberia Dollar", "LRD");
        public static SupportedCurrencies LSL => new SupportedCurrencies("Lesotho Loti", "LSL");
        public static SupportedCurrencies LYD => new SupportedCurrencies("Libya Dinar", "LYD");
        public static SupportedCurrencies MAD => new SupportedCurrencies("Morocco Dirham", "MAD");
        public static SupportedCurrencies MDL => new SupportedCurrencies("Moldova Leu", "MDL");
        public static SupportedCurrencies MGA => new SupportedCurrencies("Madagascar Ariary", "MGA");
        public static SupportedCurrencies MKD => new SupportedCurrencies("Macedonia Denar", "MKD");
        public static SupportedCurrencies MMK => new SupportedCurrencies("Myanmar (Burma) Kyat", "MMK");
        public static SupportedCurrencies MNT => new SupportedCurrencies("Mongolia Tughrik", "MNT");
        public static SupportedCurrencies MOP => new SupportedCurrencies("Macau Pataca", "MOP");
        public static SupportedCurrencies MRO => new SupportedCurrencies("Mauritania Ouguiya", "MRO");
        public static SupportedCurrencies MUR => new SupportedCurrencies("Mauritius Rupee", "MUR");
        public static SupportedCurrencies MVR => new SupportedCurrencies("Maldives (Maldive Islands) Rufiyaa", "MVR");
        public static SupportedCurrencies MWK => new SupportedCurrencies("Malawi Kwacha", "MWK");
        public static SupportedCurrencies MXN => new SupportedCurrencies("Mexico Peso", "MXN");
        public static SupportedCurrencies MYR => new SupportedCurrencies("Malaysia Ringgit", "MYR");
        public static SupportedCurrencies MZN => new SupportedCurrencies("Mozambique Metical", "MZN");
        public static SupportedCurrencies NAD => new SupportedCurrencies("Namibia Dollar", "NAD");
        public static SupportedCurrencies NGN => new SupportedCurrencies("Nigeria Naira", "NGN");
        public static SupportedCurrencies NIO => new SupportedCurrencies("Nicaragua Cordoba", "NIO");
        public static SupportedCurrencies NOK => new SupportedCurrencies("Norway Krone", "NOK");
        public static SupportedCurrencies NPR => new SupportedCurrencies("Nepal Rupee", "NPR");
        public static SupportedCurrencies NZD => new SupportedCurrencies("New Zealand Dollar", "NZD");
        public static SupportedCurrencies OMR => new SupportedCurrencies("Oman Rial", "OMR");
        public static SupportedCurrencies PAB => new SupportedCurrencies("Panama Balboa", "PAB");
        public static SupportedCurrencies PEN => new SupportedCurrencies("Peru Sol", "PEN");
        public static SupportedCurrencies PGK => new SupportedCurrencies("Papua New Guinea Kina", "PGK");
        public static SupportedCurrencies PHP => new SupportedCurrencies("Philippines Peso", "PHP");
        public static SupportedCurrencies PKR => new SupportedCurrencies("Pakistan Rupee", "PKR");
        public static SupportedCurrencies PLN => new SupportedCurrencies("Poland Zloty", "PLN");
        public static SupportedCurrencies PYG => new SupportedCurrencies("Paraguay Guarani", "PYG");
        public static SupportedCurrencies QAR => new SupportedCurrencies("Qatar Riyal", "QAR");
        public static SupportedCurrencies RON => new SupportedCurrencies("Romania New Leu", "RON");
        public static SupportedCurrencies RSD => new SupportedCurrencies("Serbia Dinar", "RSD");
        public static SupportedCurrencies RUB => new SupportedCurrencies("Russia Ruble", "RUB");
        public static SupportedCurrencies RWF => new SupportedCurrencies("Rwanda Franc", "RWF");
        public static SupportedCurrencies SAR => new SupportedCurrencies("Saudi Arabia Riyal", "SAR");
        public static SupportedCurrencies SBD => new SupportedCurrencies("Solomon Islands Dollar", "SBD");
        public static SupportedCurrencies SCR => new SupportedCurrencies("Seychelles Rupee", "SCR");
        public static SupportedCurrencies SDG => new SupportedCurrencies("Sudan Pound", "SDG");
        public static SupportedCurrencies SEK => new SupportedCurrencies("Sweden Krona", "SEK");
        public static SupportedCurrencies SGD => new SupportedCurrencies("Singapore Dollar", "SGD");
        public static SupportedCurrencies SHP => new SupportedCurrencies("Saint Helena Pound", "SHP");
        public static SupportedCurrencies SLL => new SupportedCurrencies("Sierra Leone Leone", "SLL");
        public static SupportedCurrencies SOS => new SupportedCurrencies("Somalia Shilling", "SOS");
        public static SupportedCurrencies SPL => new SupportedCurrencies("Seborga Luigino", "SPL");
        public static SupportedCurrencies SRD => new SupportedCurrencies("Suriname Dollar", "SRD");
        public static SupportedCurrencies STD => new SupportedCurrencies("São Tomé and Príncipe Dobra", "STD");
        public static SupportedCurrencies SVC => new SupportedCurrencies("El Salvador Colon", "SVC");
        public static SupportedCurrencies SYP => new SupportedCurrencies("Syria Pound", "SYP");
        public static SupportedCurrencies SZL => new SupportedCurrencies("Swaziland Lilangeni", "SZL");
        public static SupportedCurrencies THB => new SupportedCurrencies("Thailand Baht", "THB");
        public static SupportedCurrencies TJS => new SupportedCurrencies("Tajikistan Somoni", "TJS");
        public static SupportedCurrencies TMT => new SupportedCurrencies("Turkmenistan Manat", "TMT");
        public static SupportedCurrencies TND => new SupportedCurrencies("Tunisia Dinar", "TND");
        public static SupportedCurrencies TOP => new SupportedCurrencies("Tonga Pa'anga", "TOP");
        public static SupportedCurrencies TRY => new SupportedCurrencies("Turkey Lira", "TRY");
        public static SupportedCurrencies TTD => new SupportedCurrencies("Trinidad and Tobago Dollar", "TTD");
        public static SupportedCurrencies TVD => new SupportedCurrencies("Tuvalu Dollar", "TVD");
        public static SupportedCurrencies TWD => new SupportedCurrencies("Taiwan New Dollar", "TWD");
        public static SupportedCurrencies TZS => new SupportedCurrencies("Tanzania Shilling", "TZS");
        public static SupportedCurrencies UAH => new SupportedCurrencies("Ukraine Hryvnia", "UAH");
        public static SupportedCurrencies UGX => new SupportedCurrencies("Uganda Shilling", "UGX");
        public static SupportedCurrencies USD => new SupportedCurrencies("United States Dollar", "USD");
        public static SupportedCurrencies UYU => new SupportedCurrencies("Uruguay Peso", "UYD");
        public static SupportedCurrencies UZS => new SupportedCurrencies("Uzbekistan Som", "UZS");
        public static SupportedCurrencies VEF => new SupportedCurrencies("Venezuela Bolivar", "VEF");
        public static SupportedCurrencies VND => new SupportedCurrencies("Viet Nam Dong", "VND");
        public static SupportedCurrencies VUV => new SupportedCurrencies("Vanuatu Vatu", "VUV");
        public static SupportedCurrencies WST => new SupportedCurrencies("Samoa Tala", "WST");

        public static SupportedCurrencies XAF =>
            new SupportedCurrencies("Communauté Financière Africaine (BEAC) CFA Franc BEAC", "XAF");

        public static SupportedCurrencies XCD => new SupportedCurrencies("East Caribbean Dollar", "XCD");

        public static SupportedCurrencies XDR =>
            new SupportedCurrencies("International Monetary Fund (IMF) Special Drawing Rights", "XDR");

        public static SupportedCurrencies XOF =>
            new SupportedCurrencies("Communauté Financière Africaine (BCEAO) Franc", "XOF");

        public static SupportedCurrencies XPF => new SupportedCurrencies("Comptoirs Français du Pacifique (CFP) Franc", "XPF");
        public static SupportedCurrencies YER => new SupportedCurrencies("Yemen Rial", "YER");
        public static SupportedCurrencies ZAR => new SupportedCurrencies("South Africa Rand", "ZAR");
        public static SupportedCurrencies ZMW => new SupportedCurrencies("Zambia Kwacha", "ZMW");
        public static SupportedCurrencies ZWD => new SupportedCurrencies("Zimbabwe Dollar","ZWD");

        #endregion
    }
}