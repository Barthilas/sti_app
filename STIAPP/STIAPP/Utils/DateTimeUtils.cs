﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nager.Date;

namespace STIAPP.Utils
{
    class DateTimeUtils
    {
        public static DateTime AddWorkdays(DateTime originalDate, int workDays)
        {
            var year = originalDate.Year;
            var publicHolidays = DateSystem.GetPublicHoliday(year, CountryCode.CZ);
            DateTime tmpDate = originalDate;
            while (workDays > 0)
            {
                tmpDate = tmpDate.AddDays(1);
                if (tmpDate.DayOfWeek < DayOfWeek.Saturday &&
                    tmpDate.DayOfWeek > DayOfWeek.Sunday &&
                    !DateSystem.IsPublicHoliday(tmpDate, CountryCode.CZ))
                    workDays--;
            }
            return tmpDate;
        }
    }
}
