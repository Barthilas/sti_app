﻿using System.IO;
using System.Reflection;

namespace STIAPP.Utils
{
    public static class ProgramVariables
    {
        public static readonly string TemporaryFolderName = @"tmp";

        public static readonly string ExecutablePath = Path.GetDirectoryName(
            Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\\", "");

        public static readonly string TemporaryFolderPath = Path.Combine(ExecutablePath, TemporaryFolderName);
        public static readonly string XmlBanksFilePath = Path.Combine(ExecutablePath, @"history.xml");

        public static readonly string XmlUpdateFile = @"https://gitlab.com/Barthilas/sti_app/raw/master/STIAPP/STIAPP/Services/UpdateInfo.xml";
    }
}