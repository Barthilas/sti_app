﻿using System.IO;

namespace STIAPP.Utils
{
    public static class FileUtils
    {
        public static void DeleteFolderAndFiles(string path)
        {
            var di = new DirectoryInfo(path);
            foreach (var file in di.EnumerateFiles()) file.Delete();
            foreach (var dir in di.EnumerateDirectories()) dir.Delete(true);
        }
    }
}