﻿using System;
using System.IO;
using STIAPP.Enums;
using STIAPP.Interfaces;
using STIAPP.Model;

namespace STIAPP.Parsers
{
    /// <summary>
    /// Nejsem si actually jist, CNB tam má pouze "kurz" zda se jedná o prodej, výkup netuším.
    /// </summary>
    internal class CNBParser : IBankParser
    {
        private readonly char delimiter = '|';

        public DailyCurrency Decode(string file_path)
        {
            DailyCurrency result;
            using (var reader = new StreamReader(file_path))
            {
                var dateLine = reader.ReadLine().Substring(0, 10);
                result = new DailyCurrency(Convert.ToDateTime(dateLine));

                var headerLine = reader.ReadLine();
                var headers = headerLine.Split(delimiter);
                var amountIndex = 2;
                var currencyIndex = 3;
                var kurz = 4;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var data = line.Split(delimiter);
                    if (data.Length == 5)
                    {
                        var curr = SupportedCurrenciesFactory.Create(data[currencyIndex]);
                        var c = new Currency(curr, 0, float.Parse(data[kurz]), int.Parse(data[amountIndex]));
                        result.DailyCurrencyList.Add(c);
                    }

                }
            }

            return result;
        }
    }
}