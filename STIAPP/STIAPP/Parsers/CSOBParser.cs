﻿using System;
using System.IO;
using STIAPP.Enums;
using STIAPP.Interfaces;
using STIAPP.Model;

namespace STIAPP.Parsers
{
    public class CSOBParser : IBankParser
    {
        private readonly char delimiter = ';';

        public DailyCurrency Decode(string file_path)
        {
            DailyCurrency result;
            using (var reader = new StreamReader(file_path))
            {
                var dateLine = reader.ReadLine();
                result = new DailyCurrency(Convert.ToDateTime(dateLine));
                //useless
                reader.ReadLine();
                reader.ReadLine();
                var headerLine = reader.ReadLine();
                var headers = headerLine.Split(delimiter);
                var amountIndex = 1;
                var currencyIndex = 2;
                var buyIndex = 4;
                var sellIndex = 5;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var data = line.Split(delimiter);
                    var curr = SupportedCurrenciesFactory.Create(data[currencyIndex]);
                    var buy = float.Parse(data[buyIndex]);
                    var sell = float.Parse(data[sellIndex]);
                    var amount = int.Parse(data[amountIndex]);
                    var c = new Currency(curr, buy, sell,amount);
                    result.DailyCurrencyList.Add(c);
                }
            }

            return result;
        }
    }
}