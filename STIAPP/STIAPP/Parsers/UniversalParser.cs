﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using HtmlAgilityPack;
using STIAPP.Enums;
using STIAPP.Interfaces;
using STIAPP.Model;

namespace STIAPP.Parsers
{
    class UniversalParser : IBankParser
    {
        
        public DailyCurrency Decode(string filepath)
        {
            Regex rx_currency = new Regex(@"[A-Z]{3,}",
                RegexOptions.Compiled);

            Regex rx_price = new Regex(@"[-+]?[0-9]*\.?[0-9]+", RegexOptions.Compiled);

            var doc = new HtmlDocument();
            doc.Load(filepath);

            var datesTable = doc.DocumentNode.SelectNodes("//*[@id=\"leftcolumn\"]/div[3]/table[1]");
            var datesRow = datesTable.Elements("tr");

            DateTime currenDateTime = Convert.ToDateTime(datesRow.ToArray()[1].InnerText.Split(' ')[1]);

            DailyCurrency result = new DailyCurrency(currenDateTime);

            var currencyTable = doc.DocumentNode.SelectNodes("//*[@id=\"leftcolumn\"]/div[3]/table[2]");
            var currencyRows = currencyTable.Elements("tr");

            int index = 0;
            foreach (var row in currencyRows)
            {
                //avoid trash
                if (index >= 2)
                {
                    MatchCollection currency = rx_currency.Matches(row.InnerText);
                    var currencyList = (from Match m in currency select m.Value).ToList();
                    //1 buy 2 sell 0 amount == devizy
                    MatchCollection prices = rx_price.Matches(row.InnerText);
                    var pricesList = (from Match m in prices select m.Value).ToList();
                    //add to list
                    var curr = SupportedCurrenciesFactory.Create(currencyList[0]);
                    //PURE BS, FFS.
                    var buy = float.Parse(pricesList[1], CultureInfo.InvariantCulture);
                    var sell = float.Parse(pricesList[2], CultureInfo.InvariantCulture);
                    var amount = int.Parse(pricesList[0], CultureInfo.InvariantCulture);
                    var c = new Currency(curr, buy,sell,amount);
                    result.DailyCurrencyList.Add(c);

                }
                index++;
            }

            return result;
        }
    }
}
